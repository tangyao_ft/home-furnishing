package com.ruyuan2020.furnishing.tender.domain;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class CheckTrustRequestDTO extends BaseDomain {

    /**
     * 招标id
     */
    private Long tenderId;

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 托管服务金额
     */
    private BigDecimal amount;
}
