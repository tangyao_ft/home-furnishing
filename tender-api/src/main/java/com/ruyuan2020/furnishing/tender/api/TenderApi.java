package com.ruyuan2020.furnishing.tender.api;

import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.tender.domain.CheckTrustRequestDTO;

public interface TenderApi {

    /**
     * 检查支付保证金
     *
     * @param checkTrustRequestDTO 保证金信息
     */
    void checkPayTrust(CheckTrustRequestDTO checkTrustRequestDTO) throws BusinessException;

    /**
     * 完成支付保证金
     *
     * @param tenderId 招标id
     */
    void informPayTrustCompletedEvent(Long tenderId) throws BusinessException;
}
