package com.ruyuan2020.furnishing.common.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.ruyuan2020.common.domain.BaseQuery;
import com.ruyuan2020.common.domain.BasePage;

import java.io.Serializable;
import java.util.Optional;

public interface BaseDAO<D> {

    Long save(D d);

    void remove(Serializable id);

    void update(D d);

    Optional<D> getById(Serializable id);

    BasePage<D> listPage(BaseQuery query, Wrapper<D> wrapper);
}
