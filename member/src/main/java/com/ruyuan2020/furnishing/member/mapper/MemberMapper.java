package com.ruyuan2020.furnishing.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan2020.furnishing.member.domain.MemberDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MemberMapper extends BaseMapper<MemberDO> {

}
