package com.ruyuan2020.common.domain;

import java.io.Serializable;

public abstract class BaseQuery implements Serializable {

    private Integer current = 1;

    private Integer pageSize = 10;

    private String sorter;

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getSorter() {
        return sorter;
    }

    public void setSorter(String sorter) {
        this.sorter = sorter;
    }
}
