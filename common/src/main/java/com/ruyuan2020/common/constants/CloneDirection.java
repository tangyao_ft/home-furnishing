package com.ruyuan2020.common.constants;

/**
 * 克隆方向
 */
public enum CloneDirection {

    /**
     * VO -> DTO -> DO
     */
	FORWARD,

    /**
     * DO -> DTO -> VO
     */
    OPPOSITE;
}
