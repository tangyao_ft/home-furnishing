package com.ruyuan2020.furnishing.account.api;

import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.account.domain.AccountDTO;
import com.ruyuan2020.furnishing.account.domain.GoldOperationRequestDTO;

public interface AccountApi {

    /**
     * 创建账户信息
     * @param accountDTO 账户信息
     */
    void createAccount(AccountDTO accountDTO);

    /**
     * 添加金币
     *
     * @param goldOperationRequestDTO 金币信息
     */
    void addGold(GoldOperationRequestDTO goldOperationRequestDTO) throws BusinessException;

    /**
     * 支付金币
     *
     * @param goldOperationRequestDTO 金币信息
     */
    void payGold(GoldOperationRequestDTO goldOperationRequestDTO) throws BusinessException;

    /**
     * 冻结金币
     *
     * @param goldOperationRequestDTO 金币信息
     */
    void freezeGold(GoldOperationRequestDTO goldOperationRequestDTO) throws BusinessException;

    /**
     * 支付冻结金币
     *
     * @param goldOperationRequestDTO 金币信息
     */
    void payFreezeGold(GoldOperationRequestDTO goldOperationRequestDTO) throws BusinessException;
}
