package com.ruyuan2020.furnishing.thirdPay.api;

import com.ruyuan2020.furnishing.thirdPay.domain.PayRequest;

public interface ThirdPayApi {

    String sdkExecute(PayRequest payRequest);
}
