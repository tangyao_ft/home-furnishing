package com.ruyuan2020.furnishing.trade.service;

import com.ruyuan2020.common.domain.BasePage;
import com.ruyuan2020.furnishing.trade.domain.TradeLogDTO;
import com.ruyuan2020.furnishing.trade.domian.TradeDTO;
import com.ruyuan2020.furnishing.trade.domian.TradeQuery;

public interface TradeService {

    String rechargeGood(Long memberId, TradeDTO tradeDTO);

    TradeLogDTO getTradeLog(String tradeNo);

    String payTrust(TradeDTO tradeDTO);

    void informTradeCompletedEvent(String tradeNo);

    BasePage<TradeDTO> listByPage(TradeQuery tradeQuery);

    TradeDTO get(Long id);
}
