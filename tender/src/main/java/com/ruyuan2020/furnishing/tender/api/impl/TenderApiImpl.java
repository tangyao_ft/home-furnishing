package com.ruyuan2020.furnishing.tender.api.impl;

import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.tender.api.TenderApi;
import com.ruyuan2020.furnishing.tender.domain.CheckTrustRequestDTO;
import com.ruyuan2020.furnishing.tender.service.TenderService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService(version = "1.0.0", interfaceClass = TenderApi.class)
public class TenderApiImpl implements TenderApi {

    @Autowired
    private TenderService tenderService;

    @Override
    public void checkPayTrust(CheckTrustRequestDTO checkTrustRequestDTO) throws BusinessException {
        tenderService.checkPayTrust(checkTrustRequestDTO);
    }

    @Override
    public void informPayTrustCompletedEvent(Long tenderId) throws BusinessException {
        tenderService.informPayTrustCompletedEvent(tenderId);
    }
}
