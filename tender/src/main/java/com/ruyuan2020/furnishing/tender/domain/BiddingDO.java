package com.ruyuan2020.furnishing.tender.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruyuan2020.common.domain.BaseDO;
import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@TableName("ry_tender_bidding")
public class BiddingDO extends BaseDO {

    private Long tenderId;

    private Long tendererId;

    private Long bidderId;

    private String content;

    private Boolean signedFlag;

    private LocalDateTime signedTime;
}
