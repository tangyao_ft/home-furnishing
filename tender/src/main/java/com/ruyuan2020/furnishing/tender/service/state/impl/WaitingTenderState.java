package com.ruyuan2020.furnishing.tender.service.state.impl;

import com.ruyuan2020.furnishing.tender.constant.TenderStatus;
import com.ruyuan2020.furnishing.tender.dao.BiddingDAO;
import com.ruyuan2020.furnishing.tender.dao.TenderDAO;
import com.ruyuan2020.furnishing.tender.domain.TenderDO;
import com.ruyuan2020.furnishing.tender.domain.TenderDTO;
import com.ruyuan2020.furnishing.tender.service.state.TenderState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 等待投标状态
 */
@Component
public class WaitingTenderState implements TenderState {

    @Autowired
    private TenderDAO tenderDAO;

    @Autowired
    private BiddingDAO biddingDAO;

    /**
     * 等待投标状态的流转
     *
     * @param tenderDTO 招标信息
     */
    @Override
    public void doTransition(TenderDTO tenderDTO) {
        Integer biddingCount = biddingDAO.countBidding(tenderDTO.getId()) + 1;
        // 检查投标数量
        if (biddingCount.compareTo(tenderDTO.getBiddingMaxCount()) == 0) {
            tenderDTO.setStatus(TenderStatus.END_TENDER);
        }
        TenderDO tenderDO = tenderDTO.clone(TenderDO.class);
        // 更新招标状态
        tenderDAO.update(tenderDO);
    }

    @Override
    public Boolean canBid(TenderDTO tenderDTO) {
        return true;
    }

    @Override
    public Boolean canSign(TenderDTO tenderDTO) {
        return true;
    }

    @Override
    public Boolean canComplete(TenderDTO tenderDTO) {
        return false;
    }

    @Override
    public Boolean canComment(TenderDTO tenderDTO) {
        return false;
    }
}
