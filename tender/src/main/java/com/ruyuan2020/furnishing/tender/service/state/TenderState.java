package com.ruyuan2020.furnishing.tender.service.state;

import com.ruyuan2020.furnishing.tender.domain.TenderDTO;

/**
 * 招标状态接口
 */
public interface TenderState {

    /**
     * 执行招标信息流转到当前状态来的业务逻辑
     *
     * @param tenderDTO 招标信息
     */
    void doTransition(TenderDTO tenderDTO);

    /**
     * 判断当前招标信息能否执行投标
     *
     * @param tenderDTO 招标信息
     * @return 能为执行投标
     */
    Boolean canBid(TenderDTO tenderDTO);

    /**
     * 判断当前招标信息能否执行签约
     *
     * @param tenderDTO 招标信息
     * @return 能为执行签约
     */
    Boolean canSign(TenderDTO tenderDTO);

    /**
     * 判断当前招标信息能否执行完成
     *
     * @param tenderDTO 招标信息
     * @return 能为执行完成
     */
    Boolean canComplete(TenderDTO tenderDTO);

    /**
     * 判断当前招标信息能否执行评论
     *
     * @param tenderDTO 招标信息
     * @return 能为执行评论
     */
    Boolean canComment(TenderDTO tenderDTO);
}
