package com.ruyuan2020.furnishing.tender.service;

import com.ruyuan2020.furnishing.tender.domain.*;
import com.ruyuan2020.furnishing.tender.domain.CheckTrustRequestDTO;

public interface TenderService {

    Long create(TenderDTO tenderDTO);

    Long bid(BiddingDTO biddingDTO);

    void sign(SigningDTO signingDTO);

    void complete(CompletionDTO completionDTO);

    void checkPayTrust(CheckTrustRequestDTO checkTrustRequestDTO);

    void informPayTrustCompletedEvent(Long tenderId);

    TenderDTO get(Long id);
}
