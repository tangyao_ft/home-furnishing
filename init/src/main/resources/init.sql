drop
database if exists memberGroup;
create
database memberGroup charset=utf8mb4 collate utf8mb4_general_ci;
drop
database if exists accountGroup;
create
database accountGroup charset=utf8mb4 collate utf8mb4_general_ci;
drop
database if exists paymentGroup;
create
database paymentGroup charset=utf8mb4 collate utf8mb4_general_ci;
drop
database if exists tenderGroup;
create
database tenderGroup charset=utf8mb4 collate utf8mb4_general_ci;
drop
database if exists tradeGroup;
create
database tradeGroup charset=utf8mb4 collate utf8mb4_general_ci;

-- ----------------------------
-- Table structure for ry_member
-- ----------------------------
DROP TABLE IF EXISTS memberGroup.`ry_member`;
CREATE TABLE memberGroup.`ry_member`
(
    `id`           bigint   NOT NULL AUTO_INCREMENT COMMENT '主键',
    `username`     varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户名',
    `password`     char(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
    `type`         char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会员类型',
    `mail`         varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '@' COMMENT '邮箱',
    `mobile`       varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号',
    `gender`       tinyint(1) NULL DEFAULT 0 COMMENT '性别',
    `city_id`      smallint NULL DEFAULT 0 COMMENT '城市id',
    `real_name`    varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '真实姓名',
    `gmt_create`   datetime NOT NULL COMMENT '创建时间',
    `gmt_modified` datetime NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- ----------------------------
-- Table structure for ry_member_company
-- ----------------------------
DROP TABLE IF EXISTS memberGroup.`ry_member_company`;
CREATE TABLE memberGroup.`ry_member_company`
(
    `id`               bigint   NOT NULL AUTO_INCREMENT COMMENT '主键',
    `member_id`        bigint   NOT NULL COMMENT '会员id',
    `bid_quantity`     int      NOT NULL DEFAULT 0 COMMENT '参与投标数量',
    `signed_quantity`  int      NOT NULL DEFAULT 0 COMMENT '投标签约数量',
    `comment_quantity` int      NOT NULL DEFAULT 0 COMMENT '评价数量',
    `gmt_create`       datetime NOT NULL COMMENT '创建时间',
    `gmt_modified`     datetime NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `member_id`(`member_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- ----------------------------
-- Table structure for ry_account
-- ----------------------------
DROP TABLE IF EXISTS accountGroup.`ry_account`;
CREATE TABLE accountGroup.`ry_account`
(
    `id`           bigint         NOT NULL AUTO_INCREMENT COMMENT '主键',
    `member_id`    bigint         NOT NULL COMMENT '会员id',
    `gold`         decimal(10, 0) NOT NULL DEFAULT 0 COMMENT '金币',
    `freeze_gold`  decimal(10, 0) NOT NULL DEFAULT 0 COMMENT '冻结金币',
    `point`        decimal(10, 0) NOT NULL DEFAULT 0 COMMENT '积分',
    `gmt_create`   datetime       NOT NULL COMMENT '创建时间',
    `gmt_modified` datetime       NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `member_id`(`member_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- ----------------------------
-- Table structure for ry_account_log
-- ----------------------------
DROP TABLE IF EXISTS accountGroup.`ry_account_log`;
CREATE TABLE accountGroup.`ry_account_log`
(
    `id`           bigint                                             NOT NULL AUTO_INCREMENT COMMENT '主键',
    `member_id`    bigint                                             NOT NULL COMMENT '会员id',
    `type`         char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型（金币、积分）',
    `number`       decimal(10, 0)                                     NOT NULL DEFAULT 0 COMMENT '数量',
    `log`          varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志',
    `gmt_create`   datetime                                           NOT NULL COMMENT '创建时间',
    `gmt_modified` datetime                                           NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- ----------------------------
-- Table structure for ry_payment_transaction
-- ----------------------------
DROP TABLE IF EXISTS paymentGroup.`ry_payment_transaction`;
CREATE TABLE paymentGroup.`ry_payment_transaction`
(
    `id`              bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '主键',
    `member_id`       bigint                                                       NOT NULL COMMENT '会员ID',
    `member_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '会员用户名',
    `trade_no`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '交易流水号',
    `payment_method`  char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci     NOT NULL COMMENT '支付方式',
    `amount`          decimal(10, 2)                                               NOT NULL DEFAULT 0.00 COMMENT '支付金额',
    `transaction_no`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付流水号',
    `status`          tinyint(1) NOT NULL DEFAULT 0 COMMENT '交易状态',
    `gmt_finished`    datetime NULL DEFAULT NULL COMMENT '交易完成时间',
    `gmt_create`      datetime                                                     NOT NULL COMMENT '创建时间',
    `gmt_modified`    datetime                                                     NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `trade_no`(`trade_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- ----------------------------
-- Table structure for ry_tender
-- ----------------------------
DROP TABLE IF EXISTS tenderGroup.`ry_tender`;
CREATE TABLE tenderGroup.`ry_tender`
(
    `id`                bigint                                                   NOT NULL AUTO_INCREMENT COMMENT '主键',
    `category_id`       bigint                                                   NOT NULL COMMENT '招标类别',
    `budget_amount`     decimal(10, 2) NULL DEFAULT NULL COMMENT '预算金额',
    `trust_amount`      decimal(10, 2)                                           NOT NULL DEFAULT 0.00 COMMENT '托管金',
    `cost_amount`       decimal(10, 2)                                           NOT NULL COMMENT '投标费用',
    `pay_flag`          tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否支付托管金',
    `member_id`         bigint                                                   NOT NULL COMMENT '需求member id',
    `contact`           varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
    `mobile`            varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
    `city_id`           bigint NULL DEFAULT NULL COMMENT '市',
    `area_id`           bigint NULL DEFAULT NULL COMMENT '区',
    `addr`              varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
    `title`             varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '需求标题',
    `content`           varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '需求内容',
    `photo`             varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '照片',
    `status`            char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '00' COMMENT '状态',
    `bidding_max_count` int                                                      NOT NULL DEFAULT 0 COMMENT '最大投标数',
    `gmt_create`        datetime                                                 NOT NULL COMMENT '创建时间',
    `gmt_modified`      datetime                                                 NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- ----------------------------
-- Table structure for ry_tender_bidding
-- ----------------------------
DROP TABLE IF EXISTS tenderGroup.`ry_tender_bidding`;
CREATE TABLE tenderGroup.`ry_tender_bidding`
(
    `id`                      bigint   NOT NULL AUTO_INCREMENT COMMENT '主键',
    `tender_id`               bigint   NOT NULL COMMENT '招标id',
    `tenderer_id`             bigint   NOT NULL COMMENT '招标会员id',
    `bidder_id`               bigint   NOT NULL COMMENT '投标会员id',
    `content`                 varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '投标留言',
    `signed_flag`             tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否签约成功',
    `signed_time`             datetime NULL DEFAULT NULL COMMENT '签约时间',
    `gmt_create`              datetime NOT NULL COMMENT '创建时间',
    `gmt_modified`            datetime NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- ----------------------------
-- Table structure for ry_trade_log
-- ----------------------------
DROP TABLE IF EXISTS tradeGroup.`ry_trade_log`;
CREATE TABLE tradeGroup.`ry_trade_log`
(
    `id`              bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '主键',
    `member_id`       bigint                                                       NOT NULL COMMENT '支付member id',
    `trade_no`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '交易流水号',
    `payment_type`    char(2) CHARACTER SET utf8 COLLATE utf8_general_ci           NOT NULL COMMENT '支付类型',
    `payment_method`  char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci     NOT NULL COMMENT '支付方式',
    `gold`            decimal(10, 0) NULL DEFAULT NULL COMMENT '支付金币',
    `amount`          decimal(10, 2) NULL DEFAULT 0.00 COMMENT '支付金额',
    `status`          char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '交易状态',
    `tender_id`       bigint NULL DEFAULT NULL COMMENT '招标id',
    `gmt_create`      datetime                                                     NOT NULL COMMENT '创建时间',
    `gmt_modified`    datetime                                                     NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `trade_no`(`trade_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;





