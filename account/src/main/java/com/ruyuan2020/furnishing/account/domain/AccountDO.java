package com.ruyuan2020.furnishing.account.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruyuan2020.common.domain.BaseDO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@TableName("ry_account")
public class AccountDO extends BaseDO {

    private Long memberId;

    private BigDecimal gold;

    private BigDecimal freezeGold;

    private BigDecimal point;
}
