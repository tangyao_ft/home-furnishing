package com.ruyuan2020.furnishing.account.dao.impl;

import com.ruyuan2020.furnishing.account.dao.AccountLogDAO;
import com.ruyuan2020.furnishing.common.dao.impl.BaseDAOImpl;
import com.ruyuan2020.furnishing.account.domain.AccountLogDO;
import com.ruyuan2020.furnishing.account.mapper.AccountLogMapper;
import org.springframework.stereotype.Repository;

@Repository
public class AccountLogDAOImpl extends BaseDAOImpl<AccountLogMapper, AccountLogDO> implements AccountLogDAO {
}
