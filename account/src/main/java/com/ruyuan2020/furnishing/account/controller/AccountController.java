package com.ruyuan2020.furnishing.account.controller;

import com.ruyuan2020.furnishing.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 账户管理模块Controller组件
 */
@RestController
@RequestMapping("api/account")
public class AccountController {

    /**
     * 账户管理模块Service组件
     */
    @Autowired
    private AccountService accountService;
}
