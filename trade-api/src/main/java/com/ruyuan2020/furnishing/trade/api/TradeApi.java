package com.ruyuan2020.furnishing.trade.api;

import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.trade.domain.TradeLogDTO;

public interface TradeApi {

    TradeLogDTO getTradeLog(String tradeNo) throws BusinessException;

    void informTradeCompletedEvent(String tradeNo) throws BusinessException;
}
